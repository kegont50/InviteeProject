# Invitee Project API Documentation

## Endpoints

### Generate Authentication Code

**Endpoint**: `/signstart/` (POST)

Generate an authentication code for a user by providing their phone number.

### Sign Up User

**Endpoint**: `/signup/` (POST)

Sign up a user using the provided authentication code and phone number.

### Sign In User

**Endpoint**: `/signin/` (POST)

Sign in a user using the provided authentication code and phone number.

### Sign Out User

**Endpoint**: `/signout/` (POST)

Sign out the authenticated user.

### Get Profile Info

**Endpoint**: `/profile/` (GET)

Retrieve profile information of the authenticated user.

### Update Profile

**Endpoint**: `/profile/` (PUT)

Update the profile of the authenticated user, including activating an invite code.

## Setup Instructions

1. Clone the project repository to your local machine.
2. Set up a PostgreSQL database using Docker Compose:
    ```bash
    docker-compose up -d
    ```
3. Install the project dependencies using:
    ```bash
    pip install -r requirements.txt
    ```
4. Apply migrations to create the database tables:
    ```bash
    python manage.py migrate
    ```
5. Run the development server:
    ```bash
    python manage.py runserver
    ```
6. Access the API documentation at `http://localhost:8000/redoc/` to interact with the API endpoints.