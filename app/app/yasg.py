from django.urls import path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="Invitee project",
        default_version="v1",
        license=openapi.License(name="whyusoserious License"),
        x_logo={"url": "schema.yml"},
    ),
    public=True,
)