import re

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

User = get_user_model()


def check_phone_number(phone):
    return bool(re.match(r'\b\+?[7,8](\s*\d{3}\s*\d{3}\s*\d{2}\s*\d{2})\b', phone))


class HashModelBackend(ModelBackend):

    @staticmethod
    def authenticate(request, **kwargs):
        phone = request.data.get('phone')
        code = request.data.get('code')
        try:
            return User.objects.get(phone=phone, auth_code=code)
        except User.DoesNotExist:
            return None
