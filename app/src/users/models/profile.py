from django.db.models import Model, OneToOneField, CASCADE, ForeignKey, DO_NOTHING, CharField


class Profile(Model):
    user = OneToOneField('User', on_delete=CASCADE, primary_key=True)
    parent_user = ForeignKey('User', on_delete=DO_NOTHING, related_name="children", null="True", blank="True")
    invitational_code = CharField("Code", max_length=6, null="True", blank="True")

    @property
    def phone(self):
        return self.user.phone

    @property
    def get_children(self):
        return self.user.children.all()

    @property
    def get_parent(self):
        return self.parent_user
