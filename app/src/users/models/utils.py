import secrets
import string

from django.core.validators import RegexValidator

__all_ = [
    'phone_validator',
    'generate_code',
]


def generate_code(length, profile=None):
    characters = string.ascii_letters + string.digits
    while True:
        code = ''.join(secrets.choice(characters) for _ in range(length))
        if any(c.isdigit() for c in code) and any(c.isalpha() for c in code):
            if profile and not profile.objects.filter(invitational_code=code).exists():
                return code
            elif not profile:
                return code


phone_validator = RegexValidator(
    regex=r'\d{8,15}$',
    message="Phone number format: '+999999999'. Up to 15 digits allowed."
)
