from django.contrib.auth.base_user import AbstractBaseUser
from django.db.models import AutoField, CharField, BooleanField

from src.users.models.profile import Profile
from src.users.models.utils import phone_validator, generate_code


class User(AbstractBaseUser):
    id = AutoField(primary_key=True)
    phone = CharField("Phone", validators=[phone_validator], max_length=17)
    auth_code = CharField("Auth_code", max_length=4, null=True, blank=True)
    is_confirmed = BooleanField(default=False)
    password = None
    last_login = None

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        Profile(user=self, invitational_code=generate_code(6, Profile)).save()

    USERNAME_FIELD = 'id'
    REQUIRED_FIELDS = ['phone']
