from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from src.users.models import Profile
from src.users.serializer.profile import ProfileSerializer


class UserProfileView(RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Profile.objects.filter(user=self.request.user)

    def get_object(self):
        return self.get_queryset().first()

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)

        parent_user_code = request.data.get('parent_user_code')
        if parent_user_code:
            if instance.parent_user:
                return Response({'error': 'You have already activated an invite code'}, status=400)
            else:
                try:
                    parent = Profile.objects.get(invitational_code=parent_user_code)
                    instance.parent_user = parent.user
                    instance.save()
                    return Response(serializer.data, status=200)
                except Profile.DoesNotExist:
                    return Response({'error': 'Invitee code is invalid'}, status=400)
