import random
import time

from django.contrib.auth import logout, login, get_user_model
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView

from src.users.models import generate_code
from src.users.utils import HashModelBackend, check_phone_number

User = get_user_model()


class GenerateCodeView(APIView):

    def post(self, request, *args, **kwargs):
        phone = request.data.get('phone')

        if not phone:
            return Response({'error': 'Phone number is required'}, status=400)
        if not check_phone_number(phone):
            return Response({'error': 'Phone number is incorrect'}, status=400)

        auth_code = generate_code(4)

        if User.objects.filter(phone=phone).exists():
            user = User.objects.get(phone=phone)
            user.auth_code = auth_code
            user.save()
            time.sleep(random.uniform(1, 2))
            return Response({'message': f'Authentication code: {auth_code}'}, status=200)
        else:
            User.objects.create(
                phone=phone,
                auth_code=auth_code
            )
            time.sleep(random.uniform(1, 2))
            return Response({'message': f'Authentication code: {auth_code}'}, status=200)


class SignUpView(APIView):

    def post(self, request, *args, **kwargs):
        received_code = request.data.get('code')
        phone = request.data.get('phone')

        user = User.objects.get(
            phone=phone
        )

        if not phone:
            return Response({'error': 'Phone number is required'}, status=400)
        if User.objects.filter(
                phone=phone,
                is_confirmed=True
        ).exists():
            return Response({'error': 'User with this phone number is already registered'}, status=400)
        if not received_code or not user.auth_code:
            return Response({'error': 'Code is required'}, status=400)
        if received_code != user.auth_code:
            return Response({'error': 'Invalid code'}, status=400)

        user.is_confirmed = True
        user.save()
        login(request, user)

        return Response({'message': 'User created successfully'}, status=201)


class SignInView(APIView):

    def post(self, request, *args, **kwargs):
        user = HashModelBackend.authenticate(request=request)
        if user:
            if user.is_confirmed:
                login(request, user)
                token, _ = Token.objects.get_or_create(user=user)
                return Response({'message': 'Sign In success', 'token': token.key}, status=200)
            else:
                return Response({'error': 'Sign up first'}, status=400)
        else:
            return Response({'error': 'Sign In failed'}, status=400)


class SignOutView(APIView):

    def post(self, request, *args, **kwargs):
        user = request.user
        try:
            token = Token.objects.get(user=user)
            token.delete()
        except Token.DoesNotExist:
            pass
        logout(request)

        return Response(status=200)
