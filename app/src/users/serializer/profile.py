from rest_framework.fields import CharField
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from src.users.models import Profile
from src.users.models.utils import phone_validator
from users.serializer.user import UserSerializer


class ProfileShortSerializer(ModelSerializer):
    id = PrimaryKeyRelatedField(source='user.id', read_only=True)
    phone = CharField(read_only=True, source='user.phone')

    class Meta:
        model = Profile
        fields = [
            'id',
            'phone',
        ]


class ProfileSerializer(ModelSerializer):
    id = PrimaryKeyRelatedField(source='user.id', read_only=True)
    phone = CharField(source='user.phone', validators=[phone_validator], read_only=True)
    parent_user = UserSerializer(source='get_parent', required=False)
    invitees = ProfileShortSerializer(many=True, source='get_children', required=False)

    class Meta:
        model = Profile
        fields = [
            'id',
            'phone',
            'parent_user',
            'invitees',
        ]
