from rest_framework.serializers import ModelSerializer

from src.users.models import User


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'phone',
        ]
